using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatisticsText : MonoBehaviour
{
    [SerializeField]
    private LiveComponent m_PlayerLive;
    [SerializeField]
    private PlayerShootingController m_PlayerShootingController;

    [SerializeField]
    private TextMeshProUGUI m_HPText;
    [SerializeField]
    private TextMeshProUGUI m_RocketsText;

    // Update is called once per frame
    void Update()
    {
        m_HPText.text = $"HP: {m_PlayerLive.GetHp()}/{m_PlayerLive.GetTopHp()}";
        m_RocketsText.text = $"Rockets (RMB): {m_PlayerShootingController.GetRocketsCount()}/{m_PlayerShootingController.GetRocketsMaxCount()}";
    }
}
