using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public abstract class ProgressionAction
{
    //
    // Public interface.
    //
    public abstract void ActionStart();
    public abstract void ActionUpdate();
    public abstract void ActionStop();
    public abstract bool IsActionFinished();
}

[System.Serializable]
public class SpawnAction : ProgressionAction
{
    public SpawnActionObject m_Preset;

    private List<GameObject> m_spawnedObjects = new();
    private float m_nextWaveTime = 0.0f;
    private int m_waveIndex = 0;
    private int m_resolvedWaveCount = 0;

    //
    // Public interface.
    //
    public override void ActionStart()
    {
        m_nextWaveTime = Time.time + m_Preset.m_StartDelay;
        m_resolvedWaveCount = m_Preset.m_WaveCount + (int)(m_Preset.m_WaveCount * (m_Preset.m_WaveCountProgressionMultipler * (float)GameManager.GetInstance().GetInfiniteProgressionIndex()));
    }

    public override void ActionUpdate()
    {
        if (m_waveIndex < m_resolvedWaveCount && Time.time >= m_nextWaveTime)
        {
            m_spawnedObjects.AddRange(m_Preset.m_WavePreset.SpawnObjects());
            m_nextWaveTime = Time.time + m_Preset.m_StartDelay;
            ++m_waveIndex;
        }

        m_spawnedObjects.RemoveAll(obj => obj == null || !obj.activeSelf);
    }

    public override void ActionStop()
    {
        foreach (GameObject obj in m_spawnedObjects.Where(obj => obj != null))
        {
            GameObject.Destroy(obj);
        }
        m_spawnedObjects.Clear();
    }

    public override bool IsActionFinished()
    {
        if (m_Preset.m_WaitForActorsToDespawn && m_spawnedObjects.Count > 0)
        {
            return false;
        }

        return !m_Preset.m_WaitForWavesToFinish || m_waveIndex >= m_resolvedWaveCount;
    }
}
