using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Internal;

[CreateAssetMenu(fileName = "SpawnPreset", menuName = "Custom/ProgressionActions/SpawnPreset")]
public class SpawnPreset : ScriptableObject
{
    [System.Serializable]
    public struct WeightedSpawnPoint
    {
        [SerializeField]
        public string m_SpawnPointID;
        [SerializeField, Range(0,1)]
        public float m_Weight;
    }

    public GameObject m_Object;
    public WeightedSpawnPoint[] m_SpawnPoints;
    public Vector2Int m_SpawnCount;

    public List<GameObject> SpawnObjects()
    {
        if (m_SpawnPoints.Length == 0)
        {
            return new List<GameObject>();
        }

        int spawnCount = UnityEngine.Random.Range(m_SpawnCount.x, m_SpawnCount.y + 1);
        if (spawnCount == 0)
        {
            return new List<GameObject>();
        }

        List<GameObject> spawnedObjects = new();
        List<WeightedSpawnPoint> points = m_SpawnPoints.ToList();

        for (int spawnIndex = 0; spawnIndex < spawnCount; ++spawnIndex)
        {
            if (points.Count == 0)
            {
                // Not spawning more than we have points.
                break;
            }

            int pointIndex = SelectPointIndex(points);
            string pointId = points[pointIndex].m_SpawnPointID;
            SpawnPoint point = ObjectsDatabase<SpawnPoint>.FindObject(point => point.GetPointId().Equals(pointId));
            if (point != null)
            {
                GameObject spawned = Instantiate(m_Object, point.transform.position, point.transform.rotation);
                spawnedObjects.Add(spawned);
            }

            points.RemoveAt(pointIndex);
        }

        return spawnedObjects;
    }

    private static int SelectPointIndex(IList<WeightedSpawnPoint> _points)
    {
        float totalWeight = 0.0f;
        foreach (WeightedSpawnPoint point in _points)
        {
            totalWeight += point.m_Weight;
        }

        float randomWeight = UnityEngine.Random.Range(0, totalWeight);
        float currentWeight = 0.0f;
        int index = 0;

        foreach (WeightedSpawnPoint point in _points)
        {
            currentWeight += point.m_Weight;
            if (currentWeight >= randomWeight)
            {
                break;
            }
            ++index;
        }

        return Mathf.Min(index, _points.Count - 1);
    }
}
