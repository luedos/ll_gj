using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ProgressionStep
{
    [SerializeField]
    private ProgressionActionObject[] m_Actions;

    [SerializeReference, SubclassSelector]
    private ProgressionCondition[] m_Conditions;

    [SerializeField]
    private UnityEvent m_OnStarted;

    [SerializeField]
    private UnityEvent m_OnFinished;

    private ProgressionAction[] m_currentActions;

    //
    // Public interface.
    //
    public void Init()
    {
        m_currentActions = new ProgressionAction[m_Actions.Length];
        for (int actionIndex = 0;  actionIndex < m_Actions.Length; ++actionIndex)
        {
            ProgressionAction action = m_Actions[actionIndex].CreateAction();
            m_currentActions[actionIndex] = action;
            action.ActionStart();
        }

        foreach (ProgressionCondition condition in m_Conditions)
        {
            condition.InitCondition(this);
        }

        m_OnStarted.Invoke();
    }

    public bool UpdateStep()
    {
        foreach (ProgressionAction action in m_currentActions)
        {
            action.ActionUpdate();
        }

        foreach (ProgressionCondition condition in m_Conditions)
        {
            if (condition.IsConditionSatisfied())
            {
                return true;
            }
        }

        return false;
    }

    public void StopStep()
    {
        foreach (ProgressionAction action in m_currentActions)
        {
            action.ActionStop();
        }

        m_OnFinished.Invoke();
    }

    public ProgressionAction[] GetActions()
    {
        return m_currentActions;
    }
}
