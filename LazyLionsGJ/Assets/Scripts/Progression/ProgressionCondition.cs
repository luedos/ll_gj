using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public abstract class ProgressionCondition
{
    public abstract void InitCondition(ProgressionStep _step);
    public abstract bool IsConditionSatisfied();
}

[System.Serializable]
public class ProgressionConditionAll : ProgressionCondition
{
    [SerializeReference, SubclassSelector]
    private ProgressionCondition[] m_Conditions;

    public override void InitCondition(ProgressionStep _step)
    {
        foreach(ProgressionCondition condition in m_Conditions)
        {
            condition.InitCondition(_step);
        }
    }

    public override bool IsConditionSatisfied()
    {
        return m_Conditions.All(condition => condition.IsConditionSatisfied());
    }
}

[System.Serializable]
public class ProgressionConditionAny : ProgressionCondition
{
    [SerializeReference, SubclassSelector]
    private ProgressionCondition[] m_Conditions;

    public override void InitCondition(ProgressionStep _step)
    {
        foreach (ProgressionCondition condition in m_Conditions)
        {
            condition.InitCondition(_step);
        }
    }

    public override bool IsConditionSatisfied()
    {
        return m_Conditions.Length == 0 || m_Conditions.Any(condition => condition.IsConditionSatisfied());
    }
}

[System.Serializable]
public class ProgressionConditionTimer : ProgressionCondition
{
    [SerializeField]
    private float m_Time;

    private float m_endTime;

    public override void InitCondition(ProgressionStep _step)
    {
        m_endTime = Time.time + m_Time;
    }

    public override bool IsConditionSatisfied()
    {
        return Time.time > m_endTime;
    }
}


[System.Serializable]
public class ProgressionConditionAllActions : ProgressionCondition
{
    [SerializeField]
    private float m_AdditionalTime;

    private bool m_actionsFinished;
    private float m_endTime = 0.0f;
    private ProgressionStep m_currentStep;

    public override void InitCondition(ProgressionStep _step)
    {
        m_endTime = 0.0f;
        m_currentStep = _step;
        m_actionsFinished = false;
    }

    public override bool IsConditionSatisfied()
    {
        if (!m_actionsFinished && m_currentStep.GetActions().All(action => action.IsActionFinished()))
        {
            m_actionsFinished = true;
            m_endTime = Time.time + m_AdditionalTime;
        }

        return m_actionsFinished && Time.time > m_endTime;
    }
}
