using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SpawnActionObject", menuName = "Custom/ProgressionActions/SpawnActionObject")]
public class SpawnActionObject : ProgressionActionObject
{
    [SerializeField]
    public SpawnPreset m_WavePreset;

    [SerializeField]
    public int m_WaveCount;

    [SerializeField, Tooltip("Additional percent which multipled to infinite progression step count and added to m_SpawnCount")]
    public float m_WaveCountProgressionMultipler;

    [SerializeField]
    public float m_WaveDelay;

    [SerializeField]
    public float m_StartDelay;

    [SerializeField]
    public bool m_WaitForActorsToDespawn;

    [SerializeField]
    public bool m_WaitForWavesToFinish = true;

    public override ProgressionAction CreateAction()
    {
        return new SpawnAction
        {
            m_Preset = this
        };
    }
}
