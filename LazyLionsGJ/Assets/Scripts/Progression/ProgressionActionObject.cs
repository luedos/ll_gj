using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressionActionObject : ScriptableObject
{
    public virtual ProgressionAction CreateAction() { return null; }
}
