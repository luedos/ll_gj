using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D m_Rigidbody;

    [SerializeField]
    private CircleCollider2D m_CollisionCollider;

    [SerializeField]
    private IceframesComponent m_Iceframes;

    [SerializeField, Range(0, 100)]
    private float m_MovementSpeed;

    [SerializeField, Range(0, 100)]
    private float m_SpeedUpFactor;

    [SerializeField, Range(0, 100)]
    private float m_SlowDownFactor;

    [SerializeField, Range(0, 10)]
    private float m_DashDistance;

    [SerializeField, Range(0.1f, 2)]
    private float m_DashTime = 0.2f;

    [SerializeField, Range(0, 10)]
    private float m_DashTimeout;

    private PlayerInputActions m_input;

    private float m_dashStartTime = 0.0f;
    private Vector2 m_dashStopPos = Vector2.zero;
    private Vector2 m_dashStartPos = Vector2.zero;
    private bool m_inDash = false;

    //
    // Unity methods.
    //
    protected void Awake()
    {
        m_input = new PlayerInputActions();
    }

    protected void OnEnable()
    {
        m_input.Basic.Enable();
    }

    protected void OnDisable()
    {
        m_input.Basic.Disable();
    }

    protected void Update()
    {
        if (m_inDash && Time.time - m_dashStartTime > m_DashTime)
        {
            m_inDash = false;
        }
    }

    protected void FixedUpdate()
    {
        UpdateVelocity(Time.fixedDeltaTime);
    }

    protected void OnInputAction_Dash()
    {
        if (Time.time - m_dashStartTime < m_DashTimeout || !m_input.Basic.enabled || !m_input.Basic.InputAction_Movement.IsPressed())
        {
            return;
        }

        m_dashStartTime = Time.time;
        m_dashStartPos = transform.position;
        m_dashStopPos = m_dashStartPos + m_input.Basic.InputAction_Movement.ReadValue<Vector2>().normalized * m_DashDistance;
        m_Rigidbody.velocity = Vector2.zero;
        m_inDash = true;

        if (m_Iceframes != null)
        {
            m_Iceframes.SetIceframes(m_DashTime);
        }
    }

    //
    // Private interface.
    //
    private void UpdateVelocity(float _delta)
    {
        if (m_inDash)
        {
            Vector2 newPosition = Vector2.Lerp(m_dashStartPos, m_dashStopPos, (Time.time - m_dashStartTime) / m_DashTime);

            Rect positionRect = Rect.MinMaxRect(
                newPosition.x - m_CollisionCollider.radius,
                newPosition.y - m_CollisionCollider.radius,
                newPosition.x + m_CollisionCollider.radius,
                newPosition.y + m_CollisionCollider.radius);

            Vector2 minPoint = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
            Vector2 maxPoint = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

            Rect cameraRect = Rect.MinMaxRect(minPoint.x, minPoint.y, maxPoint.x, maxPoint.y);
            if (positionRect.yMax >= cameraRect.yMax || positionRect.yMin <= cameraRect.yMin || positionRect.xMax >= cameraRect.xMax || positionRect.xMin <= cameraRect.xMin)
            {
                m_inDash = false;
            }
            else
            {
                m_Rigidbody.transform.position = newPosition;
            }

            return;
        }

        Vector2 input = new Vector2();
        bool speedUp = false;

        if (m_input.Basic.enabled)
        {
            input = m_input.Basic.InputAction_Movement.ReadValue<Vector2>();
            speedUp = m_input.Basic.InputAction_Movement.IsPressed();
        }

        Vector2 desiredVelocity = input * m_MovementSpeed;
        Vector2 currentVelocity = m_Rigidbody.velocity;
        float factor = speedUp ? m_SpeedUpFactor : m_SlowDownFactor;

        m_Rigidbody.velocity = Vector2.Lerp(currentVelocity, desiredVelocity, factor * Time.fixedDeltaTime);
    }
}
