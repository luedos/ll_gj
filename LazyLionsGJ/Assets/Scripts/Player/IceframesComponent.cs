using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IceframesComponent : MonoBehaviour
{
    [SerializeField]
    private UnityEvent m_OnEnteredIceframes;

    [SerializeField]
    private UnityEvent m_OnExitIceframes;

    [SerializeField]
    private Collider2D m_DamageCollider;

    private float m_iceframesExitTime = 0.0f;
    bool m_inIceframeTime = false;

    //
    // Unity methods.
    //
    protected void Update()
    {
        if (m_inIceframeTime && Time.time >= m_iceframesExitTime)
        {
            m_DamageCollider.enabled = true;
            m_inIceframeTime = false;
            m_OnExitIceframes.Invoke();
        }
    }

    //
    // Public interface.
    //
    public void SetIceframes(float _iceframes)
    {
        if (_iceframes != 0.0f && Time.time + _iceframes > m_iceframesExitTime)
        {
            m_iceframesExitTime = Time.time + _iceframes;
            if (m_DamageCollider != null)
            {
                m_DamageCollider.enabled = false;
            }

            if (!m_inIceframeTime)
            {
                m_inIceframeTime = true;
                m_OnEnteredIceframes.Invoke();
            }
        }
    }

    public void CancelIceframes()
    {
        if (m_inIceframeTime)
        {
            m_DamageCollider.enabled = true;
            m_iceframesExitTime = 0.0f;
            m_inIceframeTime = false;
            m_OnExitIceframes.Invoke();
        }
    }
}
