using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVisual : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer m_Sprite;
    [SerializeField]
    private Color m_IceframesColor;

    private Color m_baseColor;

    //
    // Unity methods.
    //
    protected void Awake()
    {
        m_baseColor = m_Sprite.color;
    }

    //
    // Public interface.
    //

    public void EnterIceframes()
    {
        m_Sprite.color = m_IceframesColor;
    }

    public void ExitIceframes()
    {
        m_Sprite.color = m_baseColor;
    }
}
