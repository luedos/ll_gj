using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GodmodePickup : MonoBehaviour
{
    [SerializeField]
    private float m_Godmode = 5.0f;

    [SerializeField]
    private UnityEvent m_OnPickup;

    //
    // Unity methods.
    //

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<LiveComponent>(out var live))
        {
            live.Godmode(m_Godmode);
            m_OnPickup.Invoke();
        }
    }
}
