using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RocketsPickup : MonoBehaviour
{
    [SerializeField]
    private int m_Rockets = 1;

    [SerializeField]
    private UnityEvent m_OnPickup;

    //
    // Unity methods.
    //

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<PlayerShootingController>(out var controller))
        {
            controller.SupplyRockets(m_Rockets);
            m_OnPickup.Invoke();
        }
    }
}
