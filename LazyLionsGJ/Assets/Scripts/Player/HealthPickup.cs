using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthPickup : MonoBehaviour
{
    [SerializeField]
    private int m_Heal = 1;

    [SerializeField]
    private UnityEvent m_OnPickup;

    //
    // Unity methods.
    //

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<LiveComponent>(out var live))
        {
            live.Heal(m_Heal);
            m_OnPickup.Invoke();
        }
    }
}
