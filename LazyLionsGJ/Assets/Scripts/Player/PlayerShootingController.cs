using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class PlayerShootingController : MonoBehaviour
{
    [SerializeField]
    private string m_BulletId;

    [SerializeField]
    private ShootingPattern m_BulletShootingPattern;

    [SerializeField]
    private string m_RocketId;

    [SerializeField]
    private float m_BulletTimeout;

    [SerializeField]
    private float m_RocketTimeout;

    [SerializeField]
    private float m_ShootingOffset;

    [SerializeField]
    private int m_RocketsAmount;

    [SerializeField]
    private UnityEvent m_OnBulletShot;

    [SerializeField]
    private UnityEvent m_OnRocketShot;

    private float m_lastBulletTime = 0.0f;
    private float m_lastRoketTime = 0.0f;
    private PlayerInputActions m_input;
    private int m_currentRoketsAmount = 0;

    //
    // Unity methods.
    //
    protected void Awake()
    {
        m_input = new PlayerInputActions();
        m_currentRoketsAmount = m_RocketsAmount;
    }

    protected void OnEnable()
    {
        m_input.Basic.Enable();
    }

    protected void OnDisable()
    {
        m_input.Basic.Disable();
    }

    protected void Update()
    {
        if (m_input.Basic.enabled && m_input.Basic.InputAction_MouseFire.IsPressed() && Time.time - m_lastBulletTime >= m_BulletTimeout)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            SimpleShoot(mousePos);
        }
    }

    protected void OnInputAction_MouseAltFire()
    {
        if (m_input.Basic.enabled && m_currentRoketsAmount > 0 && Time.time - m_lastRoketTime >= m_RocketTimeout)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RocketAt(mousePos);
            --m_currentRoketsAmount;
        }
    }

    //
    // Public interface.
    //
    public void SimpleShoot(Vector2 _position)
    {
        if (Time.time - m_lastBulletTime < m_BulletTimeout)
        {
            return;
        }

        m_lastBulletTime = Time.time;
        m_BulletShootingPattern.SpawnProjectiles(m_BulletId, transform.position, _position, m_ShootingOffset);
        m_OnBulletShot.Invoke();
    }

    public void RocketAt(Vector2 _position)
    {
        if (Time.time - m_lastRoketTime < m_RocketTimeout)
        {
            return;
        }

        m_lastRoketTime = Time.time;
        float rotation = Vector2.SignedAngle(Vector2.right, _position - (Vector2)transform.position);
        Quaternion quaternion = Quaternion.Euler(0, 0, rotation);
        GameObject rocket = ProjectileBank.InstantiateProjectile(m_RocketId, transform.position + (quaternion * Vector2.right) * m_ShootingOffset, quaternion, _position);

        if (rocket != null && rocket.TryGetComponent<Rocket>(out var component))
        {
            component.SetDestination(_position);
        }
        m_OnRocketShot.Invoke();
    }

    public void SupplyRockets(int _rocketsCount)
    {
        m_currentRoketsAmount = Mathf.Clamp(m_currentRoketsAmount + _rocketsCount, 0, m_RocketsAmount);
    }
    public int GetRocketsMaxCount()
    {
        return m_RocketsAmount;
    }

    public int GetRocketsCount()
    {
        return m_currentRoketsAmount;
    }
}
