using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public void PrintTest(string _text)
    {
        Debug.Log($"[{gameObject.name}] {_text}.");
    }
}
