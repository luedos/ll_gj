using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Projectile", menuName = "Custom/ProjectilePreset")]
public class ProjectilePreset : ScriptableObject
{
    public GameObject m_ProjectileObject;
    public int m_HardCup = 1000;
    public int m_SoftCup = 100;
}
