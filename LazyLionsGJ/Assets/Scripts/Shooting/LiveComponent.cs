using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LiveComponent : MonoBehaviour
{
    [SerializeField]
    private int m_HP = 3;

    [SerializeField]
    private float m_IceframesOnDamage = 0.0f;

    [SerializeField]
    private IceframesComponent m_Iceframes;

    [SerializeField]
    private UnityEvent<int> m_OnDamageRecieved;

    [SerializeField]
    private UnityEvent<int> m_OnHealed;

    [SerializeField]
    private UnityEvent m_OnDead;

    [SerializeField]
    private UnityEvent m_OnEnteredGodmode;

    [SerializeField]
    private UnityEvent m_OnExitGodmode;

    private int m_currentHp;
    private float m_godmodeExitTime = 0.0f;
    bool m_ingodmode = false;

    //
    // Unity methods.
    //
    protected void Awake()
    {
        m_currentHp = m_HP;
    }
    protected void Update()
    {
        if (m_ingodmode && Time.time >= m_godmodeExitTime)
        {
            m_ingodmode = false;
            m_OnExitGodmode.Invoke();
        }
    }

    //
    // Public interface.
    //
    public void Damage(int _damage)
    {
        if (_damage <= 0)
        {
            return;
        }

        m_OnDamageRecieved.Invoke(_damage);

        if (_damage >= m_currentHp)
        {
            m_currentHp = 0;
            m_OnDead.Invoke();
        }
        else
        {
            m_currentHp -= _damage;
            if (m_Iceframes != null)
            {
                m_Iceframes.SetIceframes(m_IceframesOnDamage);
            }
        }
    }

    public void Kill()
    {
        if (m_currentHp > 0)
        {
            m_currentHp = 0;
            m_OnDead.Invoke();
        }
    }

    public void Heal(int _points)
    {
        if (m_currentHp <= 0)
        {
            return;
        }

        int difference = Mathf.Min(m_HP - m_currentHp, _points);
        m_currentHp = Mathf.Clamp(m_currentHp + _points, 0, m_HP);

        m_OnHealed.Invoke(difference);
    }

    public void Godmode(float _duration)
    {
        if (Time.time >= m_godmodeExitTime)
        {
            m_OnEnteredGodmode.Invoke();
            m_ingodmode = true;
        }

        m_godmodeExitTime = Time.time + _duration;
    }

    public void DestroyOwnerObject()
    {
        Destroy(gameObject);
    }

    public int GetHp()
    {
        return m_currentHp;
    }

    public int GetTopHp()
    {
        return m_HP;
    }
}
