using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;

[CreateAssetMenu(fileName = "ShootingPattern", menuName = "Custom/ShootingPattern")]
public class ShootingPattern : ScriptableObject
{
    [Range(0.0f, 360.0f)]
    public float ArcAngle = 0.0f;
    [Range(1, 36)]
    public int ProjectileCount = 1;

    //
    // Public interface.
    //
    public List<GameObject> SpawnProjectiles(string _projectileId, Vector3 _center, Vector3 _destination, float _offset)
    {
        List<GameObject> spawnedObjects = new List<GameObject>();
        float rotation = _destination != _center ? Vector2.SignedAngle(Vector2.right, _destination - _center) : 0.0f;
        if (ProjectileCount == 1)
        {
            Quaternion quaternion = Quaternion.Euler(0, 0, rotation);
            Vector3 direction = quaternion * Vector3.right;
            GameObject newProjectile = ProjectileBank.InstantiateProjectile(_projectileId, _center + direction * _offset, quaternion, _destination);
            if (newProjectile != null)
            {
                spawnedObjects.Add(newProjectile);
            }
            return spawnedObjects;
        }

        float startAngle = rotation - ArcAngle / 2.0f;
        for (int projectileIndex = 0; projectileIndex < ProjectileCount; projectileIndex++)
        {
            float angle = startAngle + (ArcAngle * (float)projectileIndex) / (float)(ProjectileCount);
            Quaternion quaternion = Quaternion.Euler(0, 0, angle);
            Vector3 direction = quaternion * Vector3.right;

            GameObject newProjectile = ProjectileBank.InstantiateProjectile(_projectileId, _center + direction * _offset, quaternion, _destination);
            if (newProjectile != null)
            {
                spawnedObjects.Add(newProjectile);
            }
        }

        return spawnedObjects;
    }

    public void ApplyPositionsTo(IList<GameObject> _projectiles, Vector3 _center, Vector3 _destination, float _offset)
    {
        if (_projectiles.Count == 0)
        {
            return;
        }

        float rotation = _destination != _center ? Vector2.SignedAngle(Vector2.right, _destination - _center) : 0.0f;
        if (ProjectileCount == 1)
        {
            Quaternion quaternion = Quaternion.Euler(0, 0, rotation);
            Vector3 direction = quaternion * Vector3.right;

            GameObject projectile = _projectiles[0];
            if (projectile != null)
            {
                projectile.transform.position = _center + direction * _offset;
                projectile.transform.rotation = quaternion;
            }

            return;
        }

        float startAngle = rotation - ArcAngle / 2.0f;
        for (int projectileIndex = 0; projectileIndex < ProjectileCount; projectileIndex++)
        {
            if (projectileIndex >= _projectiles.Count)
            {
                return;
            }

            float angle = startAngle + (ArcAngle * (float)projectileIndex) / (float)(ProjectileCount);
            Quaternion quaternion = Quaternion.Euler(0, 0, angle);
            Vector3 direction = quaternion * Vector3.right;

            GameObject projectile = _projectiles[projectileIndex];
            if (projectile != null)
            {
                projectile.transform.position = _center + direction * _offset;
                projectile.transform.rotation = quaternion;
            }
        }
    }
}
