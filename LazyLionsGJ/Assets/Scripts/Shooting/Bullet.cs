using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private float m_Velocity = 3.0f;

    [SerializeField]
    private Rigidbody2D m_Rigidbody;

    //
    // Unity methods.
    //

    protected void OnEnable()
    {
        m_Rigidbody.velocity = transform.right * m_Velocity;
    }
}
