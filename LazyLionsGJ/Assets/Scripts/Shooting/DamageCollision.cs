using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageCollision : MonoBehaviour
{
    [SerializeField]
    private int m_Damage = 1;

    [SerializeField]
    private UnityEvent m_OnHit;

    [SerializeField]
    private float m_DamagePeriod;

    [SerializeField]
    private bool m_OnlyReportLiveHits = false;

    private List<LiveComponent> m_damagedObjects = new();
    private float m_nextDemageTick;

    //
    // Unity methods.
    //
    protected void OnEnable()
    {
        m_nextDemageTick = Time.time + m_DamagePeriod;
        m_damagedObjects.Clear();
    }

    protected void Update()
    {
        if (m_DamagePeriod > 0.0f && Time.time > m_nextDemageTick)
        {
            m_nextDemageTick = Time.time + m_DamagePeriod;
            m_damagedObjects.RemoveAll(live => live == null || !live.enabled);
            List<LiveComponent> clonedList = new List<LiveComponent>(m_damagedObjects);
            foreach (LiveComponent live in clonedList)
            {
                live.Damage(m_Damage);
                m_OnHit.Invoke();
            }
        }
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        bool liveHit = false;
        if (collision.gameObject.TryGetComponent<LiveComponent>(out var live))
        {
            m_damagedObjects.Add(live);
            live.Damage(m_Damage);
            liveHit = true;
        }

        if (!m_OnlyReportLiveHits || liveHit)
        {
            m_OnHit.Invoke();
        }
    }

    protected void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<LiveComponent>(out var live))
        {
            m_damagedObjects.Remove(live);
        }
    }
}
