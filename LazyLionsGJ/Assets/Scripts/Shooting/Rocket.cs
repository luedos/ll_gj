using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Rocket : MonoBehaviour
{
    [SerializeField]
    private string m_BulletId;

    [SerializeField]
    private ShootingPattern m_Pattern;

    [SerializeField, Range(0.5f, 10.0f)]
    private float m_FlyTime = 3.0f;

    [SerializeField]
    private AnimationCurve m_Curve;

    [SerializeField]
    private float m_AoeRadius = 4.0f;

    [SerializeField]
    private int m_Damage = 3;

    [SerializeField]
    private UnityEvent m_OnExpload;

    [SerializeField]
    private RandomSpawner m_DestinationSpawner;

    private Vector3 m_destination;
    private Vector3 m_startPosition;
    private float m_startTime = 0.0f;
    private GameObject m_destinationSpawned;

    private bool m_destinationWasSpawned = false;

    //
    // Unity methods.
    //
    protected void FixedUpdate()
    {
        float travel = (Time.time - m_startTime) / m_FlyTime;
        transform.position = Vector3.Lerp(m_startPosition, m_destination, m_Curve.Evaluate(travel));

        if (travel >= 1.0f)
        {
            Expload();
        }
    }

    protected void OnEnable()
    {
        m_startTime = Time.time;
        m_startPosition = transform.position;
        m_destinationWasSpawned = false;
    }

    protected void Update()
    {
        if (!m_destinationWasSpawned && m_DestinationSpawner != null)
        {
            m_destinationSpawned = m_DestinationSpawner.SpawnObjectAt(m_destination, Quaternion.identity);
            m_destinationWasSpawned = true;
        }
    }

    protected void OnDisable()
    {
        if (m_destinationSpawned != null)
        {
            if (m_destinationSpawned.TryGetComponent<LiveComponent>(out LiveComponent live))
            {
                live.Kill();
            }
            else
            {
                Destroy(m_destinationSpawned);
            }
        }
    }

    //
    // Public interface.
    //
    public void SetDestination(Vector3 _destination)
    {
        m_destination = _destination;
    }

    public void Expload()
    {
        m_Pattern?.SpawnProjectiles(m_BulletId, transform.position, (m_destination - m_startPosition).normalized, 0);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, m_AoeRadius, Physics2D.GetLayerCollisionMask(gameObject.layer));
        foreach (Collider2D collider in colliders)
        {
            if (collider.TryGetComponent<LiveComponent>(out var live))
            {
                live.Damage(m_Damage);
            }
        }

        m_OnExpload.Invoke();
        for (int i = 0; i < 20; ++i)
        {
            Vector2 startPos = m_destination + Quaternion.Euler(0, 0, (360 / 20) * i) * Vector2.right * m_AoeRadius;
            Vector2 endPos = m_destination + Quaternion.Euler(0, 0, (360 / 20) * (i + 1)) * Vector2.right * m_AoeRadius;
            Debug.DrawLine(startPos, endPos, Color.red, 1.0f);
        }
    }
}
