using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileComponent : MonoBehaviour
{
    [SerializeField]
    private string m_ProjectileID = string.Empty;

    [SerializeField]
    private float m_BulletTimeout = 10.0f;

    private float m_liveStart = 0.0f;

    //
    // Unity methods.
    //
    protected void OnEnable()
    {
        m_liveStart = Time.time;
    }

    protected void Update()
    {
        if (Time.time - m_liveStart > m_BulletTimeout)
        {
            ReturnSelf();
        }
    }

    //
    // Public interface.
    //
    public string GetProjectileID()
    {
        return m_ProjectileID;
    }

    public void ReturnSelf()
    {
        ProjectileBank.ReturnObject(this);
    }
}
