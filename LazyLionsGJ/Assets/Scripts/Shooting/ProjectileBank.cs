using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class ProjectileBank : MonoBehaviour
{
    [System.Serializable]
    class PresetData
    {
        public GameObject m_ProjectileObject;
        public int m_HardCup = 1000;
        public int m_SoftCup = 100;

        [System.NonSerialized]
        public string m_projectileId;
        [System.NonSerialized]
        public List<GameObject> m_emptyObjects = new();
        [System.NonSerialized]
        private int m_spawnedObjects = 0;

        public void SpawnEmpty(ProjectileBank _bank)
        {
            if (m_spawnedObjects >= m_SoftCup)
            {
                return;
            }

            GameObject newObject = Instantiate(m_ProjectileObject, _bank.transform.position, Quaternion.identity, _bank.transform);
            newObject.SetActive(false);
            m_emptyObjects.Add( newObject );
            ++m_spawnedObjects;
        }

        public GameObject SpawnObject(ProjectileBank _bank, Vector3 _position, Quaternion _rotation, Vector3 _destination)
        {
            if (m_spawnedObjects >= m_HardCup)
            {
                return null;
            }

            GameObject newObject;

            if (m_emptyObjects.Count != 0)
            {
                newObject = m_emptyObjects[m_emptyObjects.Count - 1];
                newObject.transform.position = _position;
                newObject.transform.rotation = _rotation;

                if (newObject.TryGetComponent<Rocket>(out var rocket))
                {
                    rocket.SetDestination(_destination);
                }

                newObject.SetActive(true);
                m_emptyObjects.RemoveAt(m_emptyObjects.Count - 1);
            }
            else
            {
                newObject = Instantiate(m_ProjectileObject, _position, _rotation, _bank.transform);
                
                if (newObject.TryGetComponent<Rocket>(out var rocket))
                {
                    rocket.SetDestination(_destination);
                }

                ++m_spawnedObjects;
            }

            return newObject;
        }

        public void ReturnObject(ProjectileBank _bank, GameObject _object)
        {
            if (_object.transform.parent != _bank.transform)
            {
                _object.transform.SetParent(_bank.transform, true);
                ++m_spawnedObjects;
            }

            if (m_spawnedObjects > m_SoftCup)
            {
                Destroy(_object);
                --m_spawnedObjects;
                return;
            }

            _object.SetActive(false);
            m_emptyObjects.Add(_object);
        }
    };

    [SerializeField]
    private PresetData[] m_PresetsData;

    private static ProjectileBank s_instance;

    //
    // Unity methods.
    //
    protected void Awake()
    {
        foreach (PresetData preset in m_PresetsData)
        {
            if (preset.m_ProjectileObject == null)
            {
                continue;
            }

            ProjectileComponent component = preset.m_ProjectileObject.GetComponent<ProjectileComponent>();
            if (component == null)
            {
                continue;
            }

            preset.m_projectileId = component.GetProjectileID();

            int prespawnCount = preset.m_SoftCup / 2;
            for (int projectileIndex = 0; projectileIndex < prespawnCount; ++projectileIndex)
            {
                preset.SpawnEmpty(this);
            }
        }
    }

    protected void OnEnable()
    {
        if (s_instance == null )
        {
            s_instance = this;
        }
    }

    protected void OnDisable()
    {
        if (s_instance == this)
        {
            s_instance = null;
        }
    }

    //
    // Public interface.
    //
    public static GameObject InstantiateProjectile(string _projectileId, Vector3 _position, Quaternion _rotation, Vector3 _destination)
    {
        if (s_instance != null)
        {
            foreach (PresetData data in s_instance.m_PresetsData)
            {
                if (data.m_projectileId.Equals(_projectileId))
                {
                    return data.SpawnObject(s_instance, _position, _rotation, _destination);
                }
            }
        }

        Debug.LogError($"Can't instantiate projectile '{_projectileId}' because such Id is not found in projectile bank.");
        return null;
    }

    public static void ReturnObject(ProjectileComponent _object)
    {
        if (s_instance != null)
        {
            foreach (PresetData data in s_instance.m_PresetsData)
            {
                if (data.m_projectileId.Equals(_object.GetProjectileID()))
                {
                    data.ReturnObject(s_instance, _object.gameObject);
                    return;
                }
            }
        }

        Debug.LogError($"Destroying projectile object with id '{_object.GetProjectileID()}' but this id not found in bank.");
        Destroy(_object.gameObject);
    }
}
