using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public interface IMovementAcceleration
{
    void Init(Vector2 _position);

    Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity);
}

[System.Serializable]
public class DirectionalAcceleration : IMovementAcceleration
{
    [SerializeField, PostNormalize]
    public Vector2 m_Direction;

    public void Init(Vector2 _position) { }

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        Vector2 desiredVelocity = m_Direction * _maxVelocity;
        return desiredVelocity - _velocity;
    }
}

[System.Serializable]
public class SeekPlayerAcceleration : IMovementAcceleration
{
    [SerializeField, Tooltip("Minumal seek radius after which seeking will stop.")]
    private float m_SeekRadius = 0.0f;
    [SerializeField, Tooltip("Seek radius after which seeking will drop in speed.")]
    private float m_SoftSeekRadius = 0.0f;

    [NonSerialized]
    private float m_SeekRadiusSqr;
    [NonSerialized]
    private float m_SoftSeekRadiusSqr;
    [NonSerialized]
    private float m_RadiusDiff;

    public void Init(Vector2 _position)
    {
        m_SeekRadiusSqr = m_SeekRadius * m_SeekRadius;
        m_SoftSeekRadiusSqr = Mathf.Max(m_SoftSeekRadius * m_SoftSeekRadius, m_SeekRadiusSqr);
        m_RadiusDiff = m_SoftSeekRadiusSqr - m_SeekRadiusSqr;
    }

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        GameObject player = GameManager.GetInstance()?.GetPlayer();
        if (player == null)
        {
            return Vector2.zero;
        }

        Vector2 direction = (Vector2)player.transform.position - _position;
        float sqrMagitude = direction.sqrMagnitude;
        float speedMultipler = 1.0f;

        if (sqrMagitude < m_SeekRadiusSqr)
        {
            return Vector2.zero;
        }
        else if (sqrMagitude < m_SoftSeekRadiusSqr)
        {
            speedMultipler = (m_SoftSeekRadiusSqr - sqrMagitude) / m_RadiusDiff;
        }

        Vector2 desiredVelocity = direction.normalized * speedMultipler * _maxVelocity;
        return desiredVelocity - _velocity;
    }
}

[System.Serializable]
public class FleePlayerAcceleration : IMovementAcceleration
{
    [SerializeField, Tooltip("Radius in which enemy will flee player.")]
    public float m_FleeRadius;
    [SerializeField, Tooltip("Radius in which enemy will flee player with max speed.")]
    public float m_HardFleeRadius;

    [NonSerialized]
    private float m_FleeRadiusSqr;
    [NonSerialized]
    private float m_HardFleeRadiusSqr;
    [NonSerialized]
    private float m_RadiusDiff;

    public void Init(Vector2 _position)
    {
        m_FleeRadiusSqr = m_FleeRadius * m_FleeRadius;
        m_HardFleeRadiusSqr = Mathf.Min(m_HardFleeRadius * m_HardFleeRadius, m_FleeRadiusSqr);
        m_RadiusDiff = m_FleeRadiusSqr - m_HardFleeRadiusSqr;
    }

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        GameObject player = GameManager.GetInstance()?.GetPlayer();
        if (player == null)
        {
            return Vector2.zero;
        }

        Vector2 direction = _position - (Vector2)player.transform.position;
        float sqrMagitude = direction.sqrMagnitude;
        float speedMultipler = 1.0f;

        if (sqrMagitude > m_FleeRadiusSqr)
        {
            return Vector2.zero;
        }
        else if (sqrMagitude > m_HardFleeRadiusSqr)
        {
            speedMultipler = (sqrMagitude - m_HardFleeRadiusSqr) / m_RadiusDiff;
        }

        Vector2 desiredVelocity = direction.normalized * speedMultipler * _maxVelocity;
        return desiredVelocity - _velocity;
    }
}

[System.Serializable]
public class SeekSpawnPointAcceleration : IMovementAcceleration
{
    [SerializeField, Tooltip("Minumal seek radius after which seeking will stop.")]
    private float m_SeekRadius = 0.0f;
    [SerializeField, Tooltip("Seek radius after which seeking will drop in speed.")]
    private float m_SoftSeekRadius = 0.0f;
    [SerializeField, Tooltip("Id of spawn point which should be seeked.")]
    private string m_SpawnPointId;

    [NonSerialized]
    private float m_SeekRadiusSqr;
    [NonSerialized]
    private float m_SoftSeekRadiusSqr;
    [NonSerialized]
    private float m_RadiusDiff;
    [NonSerialized]
    private SpawnPoint m_Point;

    public void Init(Vector2 _position)
    {
        m_SeekRadiusSqr = m_SeekRadius * m_SeekRadius;
        m_SoftSeekRadiusSqr = Mathf.Max(m_SoftSeekRadius * m_SoftSeekRadius, m_SeekRadiusSqr);
        m_RadiusDiff = m_SoftSeekRadiusSqr - m_SeekRadiusSqr;
        m_Point = ObjectsDatabase<SpawnPoint>.FindObject(point => point.gameObject.name.Equals(m_SpawnPointId));
    }

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        if (m_Point == null)
        {
            return Vector2.zero;
        }

        Vector2 direction = (Vector2)m_Point.transform.position - _position;
        float sqrMagitude = direction.sqrMagnitude;
        float speedMultipler = 1.0f;

        if (sqrMagitude < m_SeekRadiusSqr)
        {
            return Vector2.zero;
        }
        else if (sqrMagitude < m_SoftSeekRadiusSqr)
        {
            speedMultipler = (m_SoftSeekRadiusSqr - sqrMagitude) / m_RadiusDiff;
        }

        Vector2 desiredVelocity = direction.normalized * speedMultipler * _maxVelocity;
        return desiredVelocity - _velocity;
    }
}

[System.Serializable]
public class FleeSpawnPointAcceleration : IMovementAcceleration
{
    [SerializeField, Tooltip("Radius in which enemy will flee player.")]
    public float m_FleeRadius;
    [SerializeField, Tooltip("Radius in which enemy will flee player with max speed.")]
    public float m_HardFleeRadius;
    [SerializeField, Tooltip("Id of spawn point which should be seeked.")]
    private string m_SpawnPointId;

    [NonSerialized]
    private float m_FleeRadiusSqr;
    [NonSerialized]
    private float m_HardFleeRadiusSqr;
    [NonSerialized]
    private float m_RadiusDiff;
    [NonSerialized]
    private SpawnPoint m_Point;

    public void Init(Vector2 _position)
    {
        m_Point = ObjectsDatabase<SpawnPoint>.FindObject(point => point.gameObject.name.Equals(m_SpawnPointId));
        m_FleeRadiusSqr = m_FleeRadius * m_FleeRadius;
        m_HardFleeRadiusSqr = Mathf.Min(m_HardFleeRadius * m_HardFleeRadius, m_FleeRadiusSqr);
        m_RadiusDiff = m_FleeRadiusSqr - m_HardFleeRadiusSqr;
    }

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        if (m_Point == null)
        {
            return Vector2.zero;
        }

        Vector2 direction = _position - (Vector2)m_Point.transform.position;
        float sqrMagitude = direction.sqrMagnitude;
        float speedMultipler = 1.0f;

        if (sqrMagitude > m_FleeRadiusSqr)
        {
            return Vector2.zero;
        }
        else if (sqrMagitude > m_HardFleeRadiusSqr)
        {
            speedMultipler = (sqrMagitude - m_HardFleeRadiusSqr) / m_RadiusDiff;
        }

        Vector2 desiredVelocity = direction.normalized * speedMultipler * _maxVelocity;
        return desiredVelocity - _velocity;
    }
}

[System.Serializable]
public class SinAcceleration : IMovementAcceleration
{
    [SerializeField, PostNormalize]
    public Vector2 m_Direction;
    [SerializeField]
    public float m_Amplitude;
    [SerializeField]
    public float m_Speed;
    [SerializeField]
    public float m_SeekingRadius;

    [NonSerialized]
    Vector2 m_startPos;
    [NonSerialized]
    private float m_startTime = 0.0f;
    [NonSerialized]
    private Quaternion m_directionQuat;

    public void Init(Vector2 _position)
    {
        m_startTime = Time.time;
        m_startPos = _position;
        m_directionQuat = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.right, m_Direction));
    }

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        Vector2 desiredPos = CalculateSinPosition(Time.time - m_startTime);

        Vector2 freeComponentDirection = m_directionQuat * Vector2.right;
        Vector2 freeComponent = freeComponentDirection * Vector2.Dot(_position, freeComponentDirection);
        Vector2 sinComponentDirection = m_directionQuat * Vector2.up;
        Vector2 sinComponent = sinComponentDirection * Vector2.Dot(desiredPos, sinComponentDirection);

        Vector2 currentPos = freeComponent + sinComponent;
        Vector2 direction = currentPos - _position;

        float distance = direction.magnitude;
        float speedMultiplier = Mathf.Min(distance / m_SeekingRadius, 1.0f);

        Vector2 desiredVelocity = ((currentPos - _position) / distance) * speedMultiplier * _maxVelocity;
        return desiredVelocity - _velocity;
    }

    private Vector2 CalculateSinPosition(float _time)
    {
        return m_startPos + (Vector2)(m_directionQuat * new Vector2(0, Mathf.Sin(_time * m_Speed) * m_Amplitude));
    }
}

[System.Serializable]
public class DirectionalSinAcceleration : IMovementAcceleration
{
    [SerializeField, PostNormalize]
    public Vector2 m_Direction;
    [SerializeField]
    public float m_Amplitude;
    [SerializeField]
    public float m_Period;
    [SerializeField]
    public float m_Speed;
    [SerializeField]
    public float m_SeekingRadius;

    [NonSerialized]
    private Vector2 m_startPos;
    [NonSerialized]
    private float m_startTime = 0.0f;
    [NonSerialized]
    private float m_timePassed = 0.0f;
    [NonSerialized]
    private float m_SeekingRadiusSqr;
    [NonSerialized]
    private Quaternion m_directionQuat;
    [NonSerialized]
    private float m_PeriodAdjusted;

    public void Init(Vector2 _position)
    {
        m_startTime = Time.time;
        m_startPos = _position;
        m_directionQuat = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.right, m_Direction));
        m_SeekingRadiusSqr = m_SeekingRadius * m_SeekingRadius;
        m_timePassed = 0.0f;
        m_PeriodAdjusted = m_Period / Mathf.PI;
    }

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        Vector2 currentPos = CalculateSinPosition(m_timePassed);
        if ((currentPos - _position).sqrMagnitude >= m_SeekingRadiusSqr)
        {
            m_startTime = Time.time - m_timePassed;
        }
        else
        {
            m_timePassed = Time.time - m_startTime;
            currentPos = CalculateSinPosition(m_timePassed);
        }

        Debug.DrawLine(_position, currentPos, Color.red);

        Vector2 desiredVelocity = (currentPos - _position).normalized * _maxVelocity;
        return desiredVelocity - _velocity;
    }

    public Vector2 CalculateSinPosition(float _time)
    {
        return m_startPos + (Vector2)(m_directionQuat * new Vector2(_time * m_PeriodAdjusted * m_Speed, Mathf.Sin(_time * m_Speed) * m_Amplitude));
    }
}

[System.Serializable]
public class StayInCameraAcceleration : IMovementAcceleration
{
    enum CameraAxis
    {
        TopDown,
        LeftRight,
        BellowTop,
        HigherThenBottom,
        FromRight,
        FromLeft
    }

    [SerializeField]
    private CameraAxis m_CameraAxis = CameraAxis.TopDown;

    [SerializeField]
    private float m_Padding;

    public void Init(Vector2 _position)
    {}

    public Vector2 CalculateAcceleration(Vector2 _position, Vector2 _velocity, float _maxVelocity)
    {
        Vector2 padding = new Vector2(m_Padding, m_Padding);
        Vector2 minPoint = (Vector2)Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)) + padding;
        Vector2 maxPoint = (Vector2)Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)) - padding;

        if (m_CameraAxis == CameraAxis.TopDown)
        {
            if (_position.x < minPoint.x)
            {
                return new Vector2(_maxVelocity, 0) - _velocity;                
            }
            else if (_position.x > maxPoint.x)
            {
                return new Vector2(-_maxVelocity, 0) - _velocity;
            }
        }
        else if (m_CameraAxis == CameraAxis.TopDown)
        {
            if (_position.y < minPoint.y)
            {
                return new Vector2(0, _maxVelocity) - _velocity;
            }
            else if (_position.y > maxPoint.y)
            {
                return new Vector2(0, -_maxVelocity) - _velocity;
            }
        }
        else if (m_CameraAxis == CameraAxis.BellowTop)
        {
            if (_position.y > maxPoint.y)
            {
                return new Vector2(0, -_maxVelocity) - _velocity;
            }
        }
        else if (m_CameraAxis == CameraAxis.HigherThenBottom)
        {
            if (_position.y < minPoint.y)
            {
                return new Vector2(0, _maxVelocity) - _velocity;
            }
        }
        else if (m_CameraAxis == CameraAxis.FromRight)
        {
            if (_position.x < minPoint.x)
            {
                return new Vector2(_maxVelocity, 0) - _velocity;
            }
        }
        else if (m_CameraAxis == CameraAxis.FromLeft)
        {
            if (_position.x > maxPoint.x)
            {
                return new Vector2(-_maxVelocity, 0) - _velocity;
            }
        }

        return Vector2.zero;
    }
}
