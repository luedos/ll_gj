using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SteeringBehavior
{
    [SerializeReference, SubclassSelector]
    public IMovementAcceleration m_Acceleration;
    [Range(0f, 1f)]
    public float m_Weight;
}

public class AccelerationMovementState : State
{
    [SerializeField]
    private SteeringBehavior[] m_Steering;

    [SerializeField]
    private Transform m_Transform;

    [SerializeField]
    private float m_MaxSpeed;

    [SerializeField]
    private float m_MaxAcceleration;

    [SerializeField, Tooltip("Amount of velocity damping if enemy is in idle state (basically how much velocity per second will be lost if in idle).")]
    private float m_InactivityTraction;

    private Vector2 m_currentVelocity;

    //
    // Public interface.
    //
    public override State OnStateActivate(IStateMachine _machine)
    {
        State baseState = base.OnStateActivate(_machine);
        if (baseState != this)
        {
            return baseState;
        }

        foreach (SteeringBehavior steering in m_Steering)
        {
            steering.m_Acceleration.Init(m_Transform.position);
        }

        return this;
    }

    public override State OnStateUpdate(float _delta)
    {
        State baseState = base.OnStateUpdate(_delta);
        if (baseState != this)
        {
            return baseState;
        }

        Vector2 acceleration = CalculateAcceleration();
        if (Mathf.Approximately(acceleration.x, 0) && Mathf.Approximately(acceleration.y, 0))
        {
            if (Mathf.Approximately(m_currentVelocity.x, 0) && Mathf.Approximately(m_currentVelocity.y, 0))
            {
                return this;
            }

            m_currentVelocity -= m_currentVelocity * Mathf.Min(m_InactivityTraction * Time.fixedDeltaTime, 1.0f);
        }
        else
        {
            m_currentVelocity += acceleration * Time.fixedDeltaTime;
            m_currentVelocity = Vector2.ClampMagnitude(m_currentVelocity, m_MaxSpeed);
        }

        m_Transform.position = m_Transform.position + (Vector3)(m_currentVelocity * Time.fixedDeltaTime);

        return this;
    }

    //
    // Private methods.
    //
    private Vector2 CalculateAcceleration()
    {
        Vector2 result = Vector2.zero;

        foreach(SteeringBehavior steering in m_Steering)
        {
            result += steering.m_Acceleration.CalculateAcceleration(m_Transform.position, m_currentVelocity, m_MaxSpeed) * steering.m_Weight;
        }

        return Vector2.ClampMagnitude(result, m_MaxAcceleration);
    }
}
