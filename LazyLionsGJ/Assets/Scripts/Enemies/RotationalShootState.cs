using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class RotationalShootState : State
{
    [SerializeField]
    private string m_BulletId;

    [SerializeField]
    private ShootingPattern m_Pattern;

    [SerializeField]
    private float m_ShotOffset;

    [SerializeField]
    private Vector2Int m_ShotsNumber;

    [SerializeField]
    private Vector2 m_ShotDelay;

    [SerializeField]
    private Vector2Int m_BurstNumber;

    [SerializeField]
    private Vector2 m_BurstDelay;

    [SerializeField]
    private float m_StartShotDelay;

    [SerializeField]
    private Vector2 m_StartAngle;

    [SerializeField]
    private Vector2 m_RotationSpeed;

    [SerializeField]
    private bool m_WaitForShootingEnd;

    [SerializeField]
    private bool m_GlueProjectiles;

    [SerializeField]
    private bool m_UseParentRotationOnGlue;

    [SerializeField]
    private UnityEvent m_OnShot;

    private int m_resolvedNumberOfShots;
    private int m_resolvedNumberOfBursts;
    private float m_resolvedShotDeleay;
    private int m_shotIndex;
    private int m_burstIndex;
    private float m_nextShotTime;
    private float m_nextBurstTime;
    private float m_resolvedRotationSpeed;
    private float m_resolvedStartAngle;
    private List<GameObject> m_lastSpawnedProjectiles;

    //
    // Public interface.
    //
    public override State OnStateActivate(IStateMachine _machine)
    {
        m_lastSpawnedProjectiles = null;
        m_shotIndex = 0;
        m_burstIndex = 1;

        float resolvedBursedDelay = Random.Range(m_BurstDelay.x, m_BurstDelay.y);

        m_nextBurstTime = Time.time + m_StartShotDelay + resolvedBursedDelay;
        m_nextShotTime = Time.time + m_StartShotDelay;

        m_resolvedShotDeleay = Random.Range(m_ShotDelay.x, m_ShotDelay.y);
        m_resolvedNumberOfShots = Random.Range(m_ShotsNumber.x, m_ShotsNumber.y + 1);
        m_resolvedNumberOfBursts = Random.Range(m_BurstNumber.x, m_BurstNumber.y + 1);

        m_resolvedRotationSpeed = Random.Range(m_RotationSpeed.x, m_RotationSpeed.y);
        m_resolvedStartAngle = transform.rotation.eulerAngles.z + Random.Range(m_StartAngle.x, m_StartAngle.y);

        State baseState = base.OnStateActivate(_machine);
        if (!m_WaitForShootingEnd && baseState != this)
        {
            return baseState;
        }

        return this;
    }

    public override void OnStateExit()
    {
        if (m_GlueProjectiles && m_lastSpawnedProjectiles != null) 
        {
            foreach (GameObject projectile in m_lastSpawnedProjectiles.Where(proj => proj != null))
            {
                if (projectile.TryGetComponent<ProjectileComponent>(out var component))
                {
                    component.ReturnSelf();
                }
                else
                {
                    GameObject.Destroy(projectile);
                }
            }
        }
        
        if (m_lastSpawnedProjectiles != null)
        {
            m_lastSpawnedProjectiles.Clear();
        }
    }

    public override State OnStateUpdate(float _delta)
    {
        State baseState = base.OnStateUpdate(_delta);
        if (baseState != this)
        {
            if (!m_WaitForShootingEnd)
            {
                return baseState;
            }

            bool shootingFinished = m_shotIndex >= m_resolvedNumberOfShots && m_burstIndex >= m_resolvedNumberOfBursts;
            bool noProjectilesLeft = !m_GlueProjectiles || m_lastSpawnedProjectiles == null || m_lastSpawnedProjectiles.All(projectile => projectile == null);
            if (shootingFinished && noProjectilesLeft)
            {
                return baseState;
            }
        }

        if (m_GlueProjectiles && m_lastSpawnedProjectiles != null && m_lastSpawnedProjectiles.Count > 0)
        {
            for (int projectileIndex = 0; projectileIndex < m_lastSpawnedProjectiles.Count; ++projectileIndex)
            {
                GameObject projectile = m_lastSpawnedProjectiles[projectileIndex];
                if (projectile != null && !projectile.activeSelf)
                {
                    m_lastSpawnedProjectiles[projectileIndex] = null;
                }
            }

            Vector3 direction = (m_UseParentRotationOnGlue 
                ? GetMachineObject().transform.rotation
                : Quaternion.Euler(0, 0, m_resolvedStartAngle + GetStateActiveTime() * m_resolvedRotationSpeed)) * Vector3.right;
            m_Pattern.ApplyPositionsTo(m_lastSpawnedProjectiles, GetMachineObject().transform.position, GetMachineObject().transform.position + direction, m_ShotOffset);
        }

        if (m_shotIndex < m_resolvedNumberOfShots && Time.time > m_nextShotTime)
        {
            ++m_shotIndex;
            m_nextShotTime = Time.time + m_resolvedShotDeleay;

            float angle = m_resolvedStartAngle + GetStateActiveTime() * m_resolvedRotationSpeed;
            Vector3 direction = Quaternion.Euler(0, 0, angle) * Vector3.right;
            m_lastSpawnedProjectiles = m_Pattern.SpawnProjectiles(m_BulletId, GetMachineObject().transform.position, GetMachineObject().transform.position + direction, m_ShotOffset);
            m_OnShot.Invoke();
        }

        if (m_shotIndex >= m_resolvedNumberOfShots && m_burstIndex < m_resolvedNumberOfBursts && Time.time > m_nextBurstTime)
        {
            ++m_burstIndex;
            m_shotIndex = 0;
            m_resolvedShotDeleay = Random.Range(m_ShotDelay.x, m_ShotDelay.y);
            m_nextShotTime = Time.time + m_StartShotDelay;

            m_nextBurstTime = Time.time + Random.Range(m_BurstDelay.x, m_BurstDelay.y);
            m_resolvedNumberOfShots = Random.Range(m_ShotsNumber.x, m_ShotsNumber.y + 1);
        }

        return this;
    }
}