using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPoint : MonoBehaviour
{
    [SerializeField]
    private string[] m_PossibleSounds;

    public void PlaySound()
    {
        if (m_PossibleSounds.Length > 0)
        {
            int index = Random.Range(0, m_PossibleSounds.Length);
            AudioManager.PlaySound(m_PossibleSounds[index]);
        }
    }
}
