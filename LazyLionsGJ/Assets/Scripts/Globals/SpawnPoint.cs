using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    //
    // Unity methods.
    //
    protected void OnEnable()
    {
        ObjectsDatabase<SpawnPoint>.RegisterObject(this);
    }
    protected void OnDisable()
    {
        ObjectsDatabase<SpawnPoint>.UnregisterObject(this);
    }

    //
    // Public interface.
    //
    public string GetPointId()
    {
        return gameObject.name;
    }
}
