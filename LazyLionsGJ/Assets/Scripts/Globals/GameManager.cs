using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private ProgressionStep[] m_Progression;

    [SerializeField]
    private ProgressionStep[] m_InfiniteProgression;

    [SerializeField]
    private GameObject m_Player;

    [SerializeField]
    private bool m_DisableProgression = false;

    static private GameManager s_instance;

    private ProgressionStep m_currentStep;
    private int m_currentStepIndex;

    //
    // Unity methods.
    //
    protected void OnEnable()
    {
        if (s_instance == null)
        {
            s_instance = this;
        }
    }

    protected void OnDisable()
    {
        if (s_instance == this)
        {
            s_instance = null;
        }
    }

    protected void Update()
    {
        if (m_DisableProgression)
        {
            return;
        }

        if (m_currentStep == null || m_currentStep.UpdateStep())
        {
            AdvanceStep();
        }
    }

    //
    // Public interface.
    //
    public static GameManager GetInstance()
    {
        return s_instance;
    }

    public GameObject GetPlayer()
    {
        return m_Player;
    }

    public int GetProgressionIndex()
    {
        return m_currentStepIndex;
    }

    public int GetInfiniteProgressionIndex()
    {
        if (m_currentStepIndex < m_Progression.Length)
        {
            return 0;
        }

        return m_currentStepIndex - m_Progression.Length;
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //
    // Private methods.
    //
    private void AdvanceStep()
    {
        ProgressionStep nextStep = null;

        if (m_currentStepIndex >= m_Progression.Length)
        {
            if (m_InfiniteProgression.Length > 0)
            {
                int infiniteProgressionIndex = (m_currentStepIndex - m_Progression.Length) % m_InfiniteProgression.Length;
                nextStep = m_InfiniteProgression[infiniteProgressionIndex];
            }
        }
        else
        {
            nextStep = m_Progression[m_currentStepIndex];
        }

        if (nextStep == null && m_currentStep ==  null)
        {
            return;
        }

        if (nextStep != null)
        {
            ++m_currentStepIndex;
        }

        if (m_currentStep != null)
        {
            m_currentStep.StopStep();
        }

        if (nextStep != null)
        {
            nextStep.Init();
        }

        Debug.Log($"[GameManager] Advanced progression to step {m_currentStepIndex}.");
        m_currentStep = nextStep;
    }
}
