using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraColliders : MonoBehaviour
{
    [SerializeField]
    private BoxCollider2D m_TopCollider;
    [SerializeField]
    private BoxCollider2D m_BottomCollider;
    [SerializeField]
    private BoxCollider2D m_LeftCollider;
    [SerializeField]
    private BoxCollider2D m_RightCollider;

    [SerializeField]
    private float m_Padding;

    //
    // Unity methods.
    //
    protected void FixedUpdate()
    {
        Vector2 minPoint = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector2 maxPoint = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        Rect rect = Rect.MinMaxRect(minPoint.x, minPoint.y, maxPoint.x, maxPoint.y);

        m_TopCollider.size = new Vector2(maxPoint.x - minPoint.x + m_Padding * 2, m_Padding);
        m_TopCollider.offset = new Vector2((maxPoint.x + minPoint.x) / 2, maxPoint.y + m_Padding / 2) - (Vector2)m_TopCollider.transform.position;

        m_BottomCollider.size = new Vector2(maxPoint.x - minPoint.x + m_Padding * 2, m_Padding);
        m_BottomCollider.offset = new Vector2((maxPoint.x + minPoint.x) / 2, minPoint.y - m_Padding / 2) - (Vector2)m_BottomCollider.transform.position;

        m_LeftCollider.size = new Vector2(m_Padding, maxPoint.y - minPoint.x + m_Padding * 2);
        m_LeftCollider.offset = new Vector2(minPoint.x - m_Padding / 2, (maxPoint.y + minPoint.y) / 2) - (Vector2)m_LeftCollider.transform.position;

        m_RightCollider.size = new Vector2(m_Padding, maxPoint.y - minPoint.x + m_Padding * 2);
        m_RightCollider.offset = new Vector2(maxPoint.x + m_Padding / 2, (maxPoint.y + minPoint.y) / 2) - (Vector2)m_RightCollider.transform.position;
    }
}
