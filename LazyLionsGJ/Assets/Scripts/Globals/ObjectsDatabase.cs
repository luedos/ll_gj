using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsDatabase<ObjectT> where ObjectT : class
{
    private static List<ObjectT> s_objects = new();

    public static ObjectT FindObject(Predicate<ObjectT> _predicate)
    {
        return s_objects.Find(_predicate);
    }

    public static void RegisterObject(ObjectT _object)
    {
        s_objects.Add(_object);
    }

    public static void UnregisterObject(ObjectT _object)
    {
        s_objects.Remove(_object);
    }
}
