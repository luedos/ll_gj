using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [System.Serializable]
    private struct NamedAudioTrack
    {
        [SerializeField]
        public AudioClip m_Clip;
        [SerializeField]
        public string m_ClipName;
    };

    [SerializeField]
    private AudioSource m_Source;

    [SerializeField]
    private NamedAudioTrack[] m_Tracks;

    private float m_nextTime = 0;
    private int m_nextClip = 0;

    private static AudioManager s_instance;

    protected void OnEnable()
    {
        s_instance = this;
    }

    protected void OnDisable()
    {
        s_instance = null;
    }

    public static void PlaySound(string _name)
    {
        AudioClip clip = s_instance?.m_Tracks.FirstOrDefault(track => track.m_ClipName.Equals(_name)).m_Clip;
        if (clip != null)
        {
            s_instance.m_Source.PlayOneShot(clip);
        }
    }
}
