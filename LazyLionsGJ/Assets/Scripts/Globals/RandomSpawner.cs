using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class RandomSpawner : MonoBehaviour
{
    [System.Serializable]
    private struct WeightedSpawnObject
    {
        [SerializeField]
        public GameObject m_SpawnObject;
        [SerializeField, Range(0,1)]
        public float m_Weight;
    }

    [SerializeField]
    private WeightedSpawnObject[] m_SpawnObject;

    [SerializeField]
    private Transform m_Parent;

    [SerializeField]
    private bool m_UseParentParameters = false;

    //
    // Public Interface.
    //
    public GameObject SpawnObject()
    {
        GameObject obj = SelectObject();

        if (obj == null)
        {
            return null;
        }

        if (m_Parent != null)
        {
            return Instantiate(obj, m_UseParentParameters ? m_Parent.position : transform.position, m_UseParentParameters ? m_Parent.rotation : transform.rotation, m_Parent);
        }
        else
        {
            return Instantiate(obj, transform.position, transform.rotation);
        }
    }
    public void SimpleSpawnObject()
    {
        SpawnObject();
    }

    public GameObject SpawnObjectAt(Vector3 _position, Quaternion _rotation)
    {
        GameObject obj = SelectObject();

        if (obj == null)
        {
            return null;
        }

        return Instantiate(obj, _position, _rotation);
    }

    //
    // Private methods.
    //
    private GameObject SelectObject()
    {
        if (m_SpawnObject.Length == 0)
        {
            return null;
        }

        float totalWeight = 0.0f;
        foreach (WeightedSpawnObject obj in m_SpawnObject)
        {
            totalWeight += obj.m_Weight;
        }

        if (Mathf.Approximately(totalWeight, 0.0f))
        {
            return null;
        }

        float randomWeight = Random.Range(0, totalWeight);
        float currentWeight = 0.0f;

        foreach (WeightedSpawnObject obj in m_SpawnObject)
        {
            currentWeight += obj.m_Weight;
            if (currentWeight > randomWeight)
            {
                return obj.m_SpawnObject;
            }
            totalWeight += obj.m_Weight;
        }

        return m_SpawnObject[m_SpawnObject.Length - 1].m_SpawnObject;
    }
}
