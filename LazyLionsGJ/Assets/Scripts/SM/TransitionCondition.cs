using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public interface IStateTransitionCondition
{
    void Init(State _state);
    bool IsSutisfied();
}

[System.Serializable]
public class AlwaysTrueCondition : IStateTransitionCondition
{
    public void Init(State _state)
    {}

    public bool IsSutisfied()
    {
        return true;
    }
}

[System.Serializable]
public class TimerCondition : IStateTransitionCondition
{
    [SerializeField]
    private Vector2 m_Time;

    [System.NonSerialized]
    private float m_exitTime;

    public void Init(State _state)
    {
        m_exitTime = Time.time + Random.Range(m_Time.x, m_Time.y);
    }

    public bool IsSutisfied()
    {
        return Time.time > m_exitTime;
    }
}

[System.Serializable]
public class AnyCondition : IStateTransitionCondition
{
    [SerializeReference, SubclassSelector]
    private IStateTransitionCondition[] m_Conditions;

    public void Init(State _state)
    {
        foreach (IStateTransitionCondition condition in m_Conditions)
        {
            condition.Init(_state);
        }
    }

    public bool IsSutisfied()
    {
        return m_Conditions.Length == 0 || m_Conditions.Any(condition =>  condition.IsSutisfied());
    }
}

[System.Serializable]
public class AllCondition : IStateTransitionCondition
{
    [SerializeReference, SubclassSelector]
    private IStateTransitionCondition[] m_Conditions;

    public void Init(State _state)
    {
        foreach (IStateTransitionCondition condition in m_Conditions)
        {
            condition.Init(_state);
        }
    }

    public bool IsSutisfied()
    {
        return m_Conditions.All(condition => condition.IsSutisfied());
    }
}

[System.Serializable]
public class IsOnCameraSideCondition : IStateTransitionCondition
{
    enum CameraSide
    {
        Left,
        Right,
        Top,
        Bottom
    }

    [SerializeField]
    private CameraSide m_Side;

    [SerializeField]
    private float m_Padding;

    [SerializeField]
    private Transform m_Object;

    public void Init(State _state)
    {}

    public bool IsSutisfied()
    {
        if (m_Object == null)
        {
            return false;
        }

        Vector2 minPoint = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector2 maxPoint = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        Rect rect = Rect.MinMaxRect(minPoint.x, minPoint.y, maxPoint.x, maxPoint.y);
        return m_Side switch
        {
            CameraSide.Left => m_Object.position.x < (rect.x - m_Padding),
            CameraSide.Right => m_Object.position.x > (rect.x + rect.size.x + m_Padding),
            CameraSide.Top => m_Object.position.y > (rect.y + m_Padding),
            CameraSide.Bottom => m_Object.position.y < (rect.y - rect.size.y - m_Padding),
            _ => false
        };
    }
}

[System.Serializable]
public class IsInsideCameraCondition : IStateTransitionCondition
{
    [SerializeField]
    private float m_Padding;

    [SerializeField]
    private Transform m_Object;

    public void Init(State _state)
    { }

    public bool IsSutisfied()
    {
        if (m_Object == null)
        {
            return false;
        }

        Vector2 minPoint = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector2 maxPoint = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        Rect rect = Rect.MinMaxRect(minPoint.x - m_Padding, minPoint.y - m_Padding, maxPoint.x + m_Padding, maxPoint.y + m_Padding);
        return rect.Contains(m_Object.transform.position);
    }
}

[System.Serializable]
public class IsOutsideCameraCondition : IStateTransitionCondition
{
    [SerializeField]
    private float m_Padding;

    [SerializeField]
    private Transform m_Object;

    public void Init(State _state)
    { }

    public bool IsSutisfied()
    {
        if (m_Object == null)
        {
            return false;
        }

        Vector2 minPoint = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector2 maxPoint = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        Rect rect = Rect.MinMaxRect(minPoint.x - m_Padding, minPoint.y - m_Padding, maxPoint.x + m_Padding, maxPoint.y + m_Padding);
        return !rect.Contains(m_Object.transform.position);
    }
}

[System.Serializable]
public class IsNearSpawnPointCondition : IStateTransitionCondition
{
    [SerializeField]
    private float m_Radius;
    [SerializeField]
    private string m_SpawnPointId;
    [SerializeField]
    private Transform m_Object;

    [System.NonSerialized]
    private float m_radiusSqr;
    [System.NonSerialized]
    private SpawnPoint m_point;

    public void Init(State _state)
    {
        m_radiusSqr = m_Radius * m_Radius;
        m_point = ObjectsDatabase<SpawnPoint>.FindObject(point => point.name.Equals(m_SpawnPointId));
    }

    public bool IsSutisfied()
    {
        if (m_point == null || m_Object == null)
        {
            return false;
        }

        return m_point != null && Vector2.SqrMagnitude((Vector2)m_Object.transform.position - (Vector2)m_point.transform.position) < m_radiusSqr;
    }
}

[System.Serializable]
public class IsFarFromSpawnPointCondition : IStateTransitionCondition
{
    [SerializeField]
    private float m_Radius;
    [SerializeField]
    private string m_SpawnPointId;
    [SerializeField]
    private Transform m_Object;

    [System.NonSerialized]
    private float m_radiusSqr;
    [System.NonSerialized]
    private SpawnPoint m_point;

    public void Init(State _state)
    {
        m_radiusSqr = m_Radius * m_Radius;
        m_point = ObjectsDatabase<SpawnPoint>.FindObject(point => point.name.Equals(m_SpawnPointId));
    }

    public bool IsSutisfied()
    {
        if (m_point == null || m_Object == null)
        {
            return false;
        }

        return m_point != null && Vector2.SqrMagnitude((Vector2)m_Object.transform.position - (Vector2)m_point.transform.position) > m_radiusSqr;
    }
}