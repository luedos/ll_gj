using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

enum StateMachineUpdate
{
    None,
    FixedUpdate,
    GameUpdate
}

public class StateMachine : MonoBehaviour, IStateMachine
{
    [SerializeField]
    private State m_StartState;

    [SerializeField]
    private bool m_AutoPlay;

    [SerializeField]
    private bool m_AutoRestart;

    [SerializeField]
    private StateMachineUpdate m_UpdateType;

    [SerializeField]
    private bool m_DrawGizmo = false;

    private State m_currentState;

    // Start is called before the first frame update
    protected void OnEnable()
    {
        if (m_StartState != null && m_AutoPlay)
        {
            ActivateState(m_StartState);
        }
    }

    protected void OnDisable()
    {
        if (m_currentState != null)
        {
            ActivateState(null);
        }
    }

    // Update is called once per frame
    protected void Update()
    {
        if (m_currentState == null)
        {
            if (m_StartState != null && m_AutoRestart)
            {
                ActivateState(m_StartState);
            }

            return;
        }

        if (m_UpdateType == StateMachineUpdate.GameUpdate)
        {
            UpdateStates(Time.deltaTime);
        }
    }

    protected void FixedUpdate()
    {
        if (m_UpdateType == StateMachineUpdate.FixedUpdate)
        {
            UpdateStates(Time.fixedDeltaTime);
        }
    }

    //
    // Public interface.
    //
    public State GetCurrentState()
    {
        return m_currentState;
    }

    public void ActivateState(State _state)
    {
        State newState = _state;

        while (newState != m_currentState)
        {
            State oldState = m_currentState;
            m_currentState = newState;

            if (oldState != null)
            {
                oldState.OnStateExit();
            }

            if (newState != null)
            {
                newState = newState.OnStateActivate(this);
            }

            OnStateChanged(oldState, newState);
        }
    }

    public GameObject GetParentObject()
    {
        return gameObject;
    }

    public IStateMachine GetParentStateMachine()
    {
        return null;
    }

    //
    // Protected methods.
    //
    protected virtual void OnStateChanged(State _oldState, State _newState)
    { }

    protected void UpdateStates(float _delta)
    {
        if (m_currentState != null)
        {
            State next = m_currentState.OnStateUpdate(_delta);
            if (next != m_currentState)
            {
                ActivateState(next);
            }
        }
    }

    protected void OnDrawGizmos()
    {
        if (m_DrawGizmo)
        {
            string state = m_currentState != null ? m_currentState.gameObject.name : "<inactive>";
            Handles.Label(transform.position + Vector3.up * 1.0f, state);
        }
    }
}
