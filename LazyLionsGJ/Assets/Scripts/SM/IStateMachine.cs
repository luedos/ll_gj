using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public interface IStateMachine
{
    State GetCurrentState();

    void ActivateState(State _state);

    GameObject GetParentObject();

    IStateMachine GetParentStateMachine();
}
