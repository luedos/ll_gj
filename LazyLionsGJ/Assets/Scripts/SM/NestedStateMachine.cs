using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class NestedStateMachine : State, IStateMachine
{
    [SerializeField]
    private State m_StartState;

    [SerializeField]
    private bool m_AutoRestart;

    private State m_currentState;

    //
    // Public interface.
    //

    public override State OnStateActivate(IStateMachine _machine)
    {
        State nextState = base.OnStateActivate(_machine);
        if (nextState != this)
        {
            return nextState;
        }

        if (m_StartState != null)
        {
            ActivateState(m_StartState);
        }

        return this;
    }

    public override void OnStateExit()
    {
        if (m_currentState != null)
        {
            ActivateState(null);
        }
    }

    public override State OnStateUpdate(float _delta)
    {
        State nextState = base.OnStateUpdate(_delta);
        if (nextState != this)
        {
            return nextState;
        }

        if (m_currentState == null)
        {
            if (m_StartState != null && m_AutoRestart)
            {
                ActivateState(m_StartState);
            }

            return this;
        }

        UpdateStates(_delta);
        return this;
    }

    public State GetCurrentState()
    {
        return m_currentState;
    }

    public void ActivateState(State _state)
    {
        State newState = _state;

        while (newState != m_currentState)
        {
            State oldState = m_currentState;
            m_currentState = newState;

            if (oldState != null)
            {
                oldState.OnStateExit();
            }

            if (newState != null)
            {
                newState = newState.OnStateActivate(this);
            }

            OnStateChanged(oldState, newState);
        }
    }

    public GameObject GetParentObject()
    {
        return gameObject;
    }

    public IStateMachine GetParentStateMachine()
    {
        return GetMachine();
    }

    //
    // Protected methods.
    //
    protected virtual void OnStateChanged(State _oldState, State _newState)
    { }

    protected void UpdateStates(float _delta)
    {
        if (m_currentState != null)
        {
            State next = m_currentState.OnStateUpdate(_delta);
            if (next != m_currentState)
            {
                ActivateState(next);
            }
        }
    }

    protected void OnDrawGizmos()
    {
        string state = m_currentState != null ? m_currentState.gameObject.name : "<inactive>";
        Handles.Label(transform.position + Vector3.up * 1.0f, state);
    }
}
