using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public interface ITransition
{
    void Init(State _state);

    State GetNextState(State _currentState);
}

[System.Serializable]
public class SimpleTransition : ITransition
{
    [SerializeField]
    private State m_NextState;

    [SerializeReference, SubclassSelector]
    private IStateTransitionCondition m_Condition;

    public void Init(State _state)
    {
        if (m_Condition != null)
        {
            m_Condition.Init(_state);
        }
    }

    public State GetNextState(State _currentState)
    {
        if (m_Condition != null && m_Condition.IsSutisfied())
        {
            return m_NextState;
        }

        return _currentState;
    }
}

[System.Serializable]
public class RandomTransition : ITransition
{
    [System.Serializable]
    private struct WeightedState
    {
        [SerializeField]
        public State m_NextState;
        [SerializeField, Range(0, 1)]
        public float m_Weight;
    }

    [SerializeField]
    private WeightedState[] m_NextStates;

    [SerializeReference, SubclassSelector]
    private IStateTransitionCondition m_Condition;

    [System.NonSerialized]
    private float m_totalWeight;

    public void Init(State _state)
    {
        if (m_Condition != null)
        {
            m_Condition.Init(_state);
        }

        m_totalWeight = 0.0f;
        foreach (WeightedState state in m_NextStates)
        {
            m_totalWeight += state.m_Weight;
        }
    }

    public State GetNextState(State _currentState)
    {
        if (m_Condition != null && m_Condition.IsSutisfied())
        {
            if (m_NextStates.Length == 0)
            {
                return null;
            }

            float randomWeight = Random.Range(0.0f, m_totalWeight);
            float currentWeight = 0.0f;
            foreach (WeightedState state in m_NextStates)
            {
                currentWeight += state.m_Weight;
                if (currentWeight >= randomWeight)
                {
                    return state.m_NextState;
                }
            }

            return m_NextStates[m_NextStates.Length - 1].m_NextState;
        }

        return _currentState;
    }
}

public class State : MonoBehaviour
{
    [SerializeReference, SubclassSelector]
    private ITransition[] m_Transitions;
    
    [SerializeField]
    private UnityEvent m_OnStateStarted;
    [SerializeField]
    private UnityEvent m_OnStateFinished;

    private IStateMachine m_machine;
    private float m_stateStartTime;

    //
    // Public interface.
    //
    public IStateMachine GetMachine()
    { 
        return m_machine;
    }
    public GameObject GetMachineObject()
    {
        return m_machine?.GetParentObject();
    }

    public bool IsStateActive()
    {
        return m_machine != null;
    }

    public float GetStateActiveTime()
    {
        return Time.time - m_stateStartTime;
    }

    public virtual State OnStateActivate(IStateMachine _machine)
    {
        m_machine = _machine;
        m_stateStartTime = Time.time;
        m_OnStateStarted.Invoke();
        enabled = true;

        foreach (ITransition transition in m_Transitions)
        {
            transition.Init(this);
        }

        foreach (ITransition transition in m_Transitions)
        {
            State nextState = transition.GetNextState(this);
            if (nextState != this)
            {
                return nextState;
            }
        }

        return this;
    }

    public virtual State OnStateUpdate(float _delta)
    {
        foreach (ITransition transition in m_Transitions)
        {
            State nextState = transition.GetNextState(this);
            if (nextState != this)
            {
                return nextState;
            }
        }

        return this;
    }

    public virtual void OnStateExit()
    {
        enabled = false;
        m_machine = null;
        m_OnStateFinished.Invoke();
    }
}
